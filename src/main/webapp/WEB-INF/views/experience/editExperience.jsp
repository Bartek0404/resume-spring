<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:useBean id="now" class="java.util.Date"/>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="../dynamic/css.jspf" %>
<%@include file="../dynamic/navigationMain.jspf" %>


<!-- Page Content-->
<div class="container-fluid p-0">
    <!-- Experience-->
    <section class="resume-section" id="experience">
        <div class="resume-section-content">
            <h2 class="mb-5">Edit experience</h2>
            <form method="post" action='<c:url value="/saveExperience/${experience.id}"/>'>
                <div class="row">
                    <div class="col-xl-12 col-md-12 mb-12">
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Job position</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" name="jobPosition" value="${experience.jobPosition}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Company name</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" name="companyName" value="${experience.companyName}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Job description</label>
                                    <div class="col-9">
                                        <textarea class="form-control" rows="5" name="jobDescription">${experience.jobDescription}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Date (from)</label>
                                    <div class="col-9">
                                        <input class="form-control" type="date" name="startDate" max="3000-12-31" value="<fmt:formatDate pattern = "yyyy-MM-dd" value = "${experience.startDate}" />">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3 col-form-label">Date (to)</label>
                                    <div class="col-9">
                                        <input class="form-control" type="date" name="endDate" max="3000-12-31" value="<fmt:formatDate pattern = "yyyy-MM-dd" value = "${experience.endDate}" />">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <sec:authorize access="hasAnyAuthority('ADMIN')">
                    <input class="btn btn-info btn-const-100" type="submit" value="Save" id="searchButton"></input>
                    <a class="btn btn-success btn-const-100" href='<c:url value="/experience"/>'>Back</a>
                </sec:authorize>
            </form>
        </div>
    </section>
</div>

<%@include file="../dynamic/javaScript.jspf" %>
>
</body>
</html>
