<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:useBean id="now" class="java.util.Date"/>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="../dynamic/css.jspf" %>
<%@include file="../dynamic/navigationMain.jspf" %>


<!-- Page Content-->
<div class="container-fluid p-0">
    <!-- Experience-->
    <section class="resume-section" id="experience">
        <div class="resume-section-content">
            <h2 class="mb-5">Experience</h2>

            <c:forEach items="${experience}" var="title">
                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                    <div class="flex-grow-1">
                        <h3 class="mb-0">${title.jobPosition}</h3>
                        <div class="subheading mb-3">${title.companyName}</div>
                        <p>${title.jobDescription}</p>
                    </div>
                    <div class="flex-shrink-0">
                        <span class="text-primary">
                            <fmt:setLocale value="en_US"/> <fmt:formatDate value="${title.startDate}" type="date" pattern="MMMM yyyy"/> - <fmt:formatDate value="${title.endDate}" type="date" pattern="MMMM yyyy"/>
                        </span>
                    </div>
                </div>
                <sec:authorize access="hasAnyAuthority('ADMIN')">
                    <span class="spanFormat">
                    <a href='<c:url value="/editExperience/${title.id}"/>' class="btn btn-outline-info btn-const-100">Edit</a>
                    </span>
                    <span class="spanFormat">
                    <form method="post" action='<c:url value="/removeExperience/${title.id}"/>'>
                        <input type="submit" class="btn btn-outline-danger btn-const-100" value="Remove"/>
                    </form>
                    </span>
                    <br>
                </sec:authorize>

            </c:forEach>

            <sec:authorize access="hasAnyAuthority('ADMIN')">
                <a href='<c:url value="/addExperience"/>' class="btn btn-info btn-const-200">
                    <span class="text">Add new experience</span>
                </a>
            </sec:authorize>

        </div>
    </section>
</div>

<%@include file="../dynamic/javaScript.jspf" %>
>
</body>
</html>
