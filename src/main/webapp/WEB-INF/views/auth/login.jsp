<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="now" class="java.util.Date"/>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="../dynamic/css.jspf" %>
<%@include file="../dynamic/navigationMain.jspf" %>


<!-- Page Content-->
<div class="container-fluid p-0">
    <!-- Interests-->
    <section class="resume-section" id="interests">
        <div class="resume-section-content">
            <h2 class="mb-5">Login Page</h2>

            <p class="mb-0">Enter your username and password:</p><br>
            <form method="post" action="/login">
            <div class="card-header py-3">
                <div class="form-group row">
                    <label class="col-2 col-form-label">Username</label>
                    <div class="col-4">
                        <input class="form-control" type="text" name="username" placeholder="Username">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-2 col-form-label">Password</label>
                    <div class="col-4">
                        <input class="form-control" type="password" name="password" placeholder="Password">
                    </div>
                </div>
            </div>
                <br>
                <input class="btn btn-success pull-left" type="submit" value="Log in" id="searchButton"></input>
            </form>
        </div>
    </section>
</div>

<%@include file="../dynamic/javaScript.jspf" %>
>
</body>
</html>
