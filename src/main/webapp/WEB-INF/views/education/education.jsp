<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<jsp:useBean id="now" class="java.util.Date"/>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="../dynamic/css.jspf" %>
<%@include file="../dynamic/navigationMain.jspf" %>


<!-- Page Content-->
<div class="container-fluid p-0">
    <!-- Education-->
    <section class="resume-section" id="education">
        <div class="resume-section-content">
            <h2 class="mb-5">Education</h2>
            <c:forEach items="${education}" var="title">

                <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                    <div class="flex-grow-1">
                        <h3 class="mb-0">${title.institutionName}</h3>
                        <div class="subheading mb-3">${title.graduationTitle}</div>
                        <div>${title.graduationSpecialization}</div>
                        <p>${title.gpa}</p>
                    </div>
                    <div class="flex-shrink-0"><span class="text-primary"><fmt:formatDate value="${title.startDate}" type="date" pattern="MMMM yyyy"/> - <fmt:formatDate value="${title.endDate}" type="date" pattern="MMMM yyyy"/></span></div>
                </div>
            </c:forEach>
        </div>
    </section>
</div>

<%@include file="../dynamic/javaScript.jspf" %>
>
</body>
</html>
