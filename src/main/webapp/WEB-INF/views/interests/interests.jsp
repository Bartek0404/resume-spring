<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<jsp:useBean id="now" class="java.util.Date"/>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="../dynamic/css.jspf" %>
<%@include file="../dynamic/navigationMain.jspf" %>


<!-- Page Content-->
<div class="container-fluid p-0">
    <!-- Interests-->
    <section class="resume-section" id="interests">
        <div class="resume-section-content">
            <h2 class="mb-5">Interests</h2>

            <c:forEach items="${interest}" var="title">
                <p class="mb-0">${title.interestDescription}</p>
                <sec:authorize access="hasAnyAuthority('ADMIN')">
                    <span class="spanFormat">
                    <a href='<c:url value="/editInterest/${title.id}"/>' class="btn btn-outline-info btn-const-100">Edit</a>
                    </span>
                    <span class="spanFormat">
                    <form method="post" action='<c:url value="/removeInterest/${title.id}"/>'>
                        <input type="submit" class="btn btn-outline-danger btn-const-100" value="Remove"/>
                    </form>
                    </span>
                    <br>
                </sec:authorize>
                <br>
            </c:forEach>

            <sec:authorize access="hasAnyAuthority('ADMIN')">
                <a href='<c:url value="/addInterest"/>' class="btn btn-info btn-const-200">
                    <span class="text">Add new interest</span>
                </a>
            </sec:authorize>


        </div>
    </section>
    <!-- The Modal -->
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Czy na pewno chcesz usunąć osobę ?</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Jeżeli usuniesz to już nie będzie odwrotu
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <form method="post" action='<c:url value="/removeInterest/${title.id}"/>'>
                        <input type="submit" class="btn btn-danger pull-left" value="Yes"/>
                    </form>
                </div>

            </div>

        </div>
    </div>
</div>


<%@include file="../dynamic/javaScript.jspf" %>
>
</body>
</html>
