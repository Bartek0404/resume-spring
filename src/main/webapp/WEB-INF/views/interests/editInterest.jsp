<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:useBean id="now" class="java.util.Date"/>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="../dynamic/css.jspf" %>
<%@include file="../dynamic/navigationMain.jspf" %>


<!-- Page Content-->
<div class="container-fluid p-0">
    <!-- Interests-->
    <section class="resume-section" id="interests">
        <div class="resume-section-content">
            <h2 class="mb-5">Edit interest</h2>

            <form method="post" action='<c:url value="/saveInterest/${interest.id}"/>'>
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-10">
                                <textarea class="form-control" rows="5"
                                          name="interestDescription">${interest.interestDescription}
                                </textarea>
                            <br>
                        </div>
                    </div>
                </div>
                <br>
                <%--<sec:authorize access="hasAnyAuthority('ADMIN')">--%>
                <input class="btn btn-info btn-const-100" type="submit" value="Save" id="searchButton"></input>
                <%-- </sec:authorize>--%>
                <a class="btn btn-success btn-const-100" href='<c:url value="/interests"/>'>Back</a>
            </form>
        </div>
    </section>
</div>

<%@include file="../dynamic/javaScript.jspf" %>
>
</body>
</html>
