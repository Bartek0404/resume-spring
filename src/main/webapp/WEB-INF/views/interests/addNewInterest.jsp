<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<jsp:useBean id="now" class="java.util.Date"/>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="../dynamic/css.jspf" %>
<%@include file="../dynamic/navigationMain.jspf" %>


<!-- Page Content-->
<div class="container-fluid p-0">
    <!-- Interests-->
    <section class="resume-section" id="interests">
        <div class="resume-section-content">
            <h2 class="mb-5">Add new interest</h2>

            <form method="post" action='<c:url value="/addInterest"/>'>
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <div class="col-10">
                                <textarea class="form-control" rows="5" name="interestDescription"
                                          placeholder="Describe your interest here..."></textarea>

                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <sec:authorize access="hasAnyAuthority('ADMIN')">
                    <input class="btn btn-info btn-const-100" type="submit" value="Add" id="searchButton"></input>
                    <%--                        <form method="get" action='<c:url value="/interests"/>'>
                                                <input class="btn btn-success btn-const-100" type="submit" value="Back" id="backButton"></input>
                                            </form>--%>
                    <a class="btn btn-success btn-const-100" href='<c:url value="/interests"/>'>Back</a>
                </sec:authorize>

            </form>

        </div>
    </section>
</div>

<%@include file="../dynamic/javaScript.jspf" %>
>
</body>
</html>
