package com.example.resumespring.model;

import javax.persistence.*;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Entity
@RequiredArgsConstructor
@Getter
@Setter
public class Experience {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "job_position")
    private String jobPosition;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "job_description")
    private String jobDescription;

    @Column(name = "start_date")
    @DateTimeFormat(pattern = "MMMM-YYYY")
    private Date startDate;

    @Column(name = "end_date")
    @DateTimeFormat(pattern = "MMMM-YYYY")
    private Date endDate;

}
