package com.example.resumespring.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;

import java.util.Date;

@Entity
@RequiredArgsConstructor
@Getter
@Setter
public class Education {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "institution_name")
    private String institutionName;

    @Column(name = "graduation_title")
    private String graduationTitle;

    @Column(name = "graduation_specialization")
    private String graduationSpecialization;

    @Column(name = "gpa")
    private Float gpa;

    @Column(name = "start_date")
    @DateTimeFormat(pattern = "MMMM-YYYY")
    private Date startDate;

    @Column(name = "end_date")
    @DateTimeFormat(pattern = "MMMM-YYYY")
    private Date endDate;

}
