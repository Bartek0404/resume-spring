package com.example.resumespring.repository;

import com.example.resumespring.model.Interest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface InterestRepository extends JpaRepository<Interest, Long> {
}
