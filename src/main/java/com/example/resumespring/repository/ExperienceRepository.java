package com.example.resumespring.repository;

import com.example.resumespring.model.Experience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ExperienceRepository extends JpaRepository<Experience, Long> {
}
