package com.example.resumespring.controller;

import com.example.resumespring.model.Education;
import com.example.resumespring.service.EducationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequiredArgsConstructor

public class EducationController {

    private final EducationService educationService;
    @GetMapping("/education")
    public String getEducation(Model model) {
        List<Education> educationsList = educationService.getEducationsList();
        model.addAttribute("education", educationsList);
        return "education/education";
    }
    @GetMapping("/addEducation")
    public String getAddEducation() {
        return "education/addEducation";
    }

    @PostMapping("/addEducation")
    public RedirectView postAddEducation(Education education) {
        educationService.addEducation(education);
        return new RedirectView("education/education");
    }


}
