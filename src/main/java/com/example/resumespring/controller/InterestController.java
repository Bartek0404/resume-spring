package com.example.resumespring.controller;

import com.example.resumespring.model.Interest;
import com.example.resumespring.service.InterestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequiredArgsConstructor

public class InterestController {

    private final InterestService interestService;
    @GetMapping("/interests")
    public String getInterests(Model model) {
        List<Interest> interestsList = interestService.getInterestsList();
        model.addAttribute("interest", interestsList);
        return "interests/interests";
    }

    @GetMapping("/addInterest")
    public String getAddInterest() {
        return "interests/addNewInterest";
    }

    @PostMapping("/addInterest")
    public RedirectView postAddInterest(Interest interest) {
        interestService.addInterest(interest);
        return new RedirectView("interests");
    }
    @GetMapping("/editInterest/{id}")
    public String getEditInterest(@PathVariable("id") Long id, Model model) {
        Interest interest = interestService.getInterestById(id);
        model.addAttribute("interest", interest);
        return "interests/editInterest";
    }
    @PostMapping("/saveInterest/{id}")
    public RedirectView postEditInterest(@PathVariable("id") Long id, Interest editInterest) {
        interestService.editInterest(editInterest);
        return new RedirectView("/interests");
    }
    @PostMapping("/removeInterest/{id}")
    public RedirectView postRemoveInterest(@PathVariable("id") Long id) {
       interestService.removeInterest(id);
        return new RedirectView("/interests");
    }
}
