package com.example.resumespring.controller;

import com.example.resumespring.model.Experience;
import com.example.resumespring.service.ExperienceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

@Controller
@RequiredArgsConstructor

public class ExperienceController {

    private final ExperienceService experienceService;
    @GetMapping("/experience")
    public String getExperience(Model model) {
        List<Experience> experienceList = experienceService.getExperiencesList();
        model.addAttribute("experience", experienceList);
        return "experience/experience";
    }

    @GetMapping("/addExperience")
    public String getAddExperience() {
        return "experience/addNewExperience";
    }

    @PostMapping("/addExperience")
    public RedirectView postAddExperience(Experience experience) {
        experienceService.addExperience(experience);
        return new RedirectView("experience");
    }
    @GetMapping("/editExperience/{id}")
    public String getEditExperience(@PathVariable("id") Long id, Model model) {
        Experience experience = experienceService.getExperienceById(id);
        model.addAttribute("experience", experience);
        return "experience/editExperience";
    }
    @PostMapping("/saveExperience/{id}")
    public RedirectView postEditExperience(@PathVariable("id") Long id, Experience editExperience) {
        experienceService.editExperience(editExperience);
        return new RedirectView("/experience");
    }
    @PostMapping("/removeExperience/{id}")
    public RedirectView postRemoveExperience(@PathVariable("id") Long id) {
        experienceService.removeExperience(id);
        return new RedirectView("/experience");
    }
}
