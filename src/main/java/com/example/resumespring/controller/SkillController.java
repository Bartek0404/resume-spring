package com.example.resumespring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SkillController {
    @Autowired
    PasswordEncoder passwordEncoder;
    @GetMapping("/skills")
    public String getSkills() {
        System.out.println(passwordEncoder.encode("test"));
        return "skills/skills";
    }
}
