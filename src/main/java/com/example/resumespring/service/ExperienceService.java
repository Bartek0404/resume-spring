package com.example.resumespring.service;

import com.example.resumespring.model.Experience;
import com.example.resumespring.repository.ExperienceRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ExperienceService {
    private final ExperienceRepository experienceRepository;

    /*public List<Experience> getExperiencesList(){
        return experienceRepository.findAll();
    }*/

    public List<Experience> getExperiencesList(){
        return experienceRepository.findAll(Sort.by(Sort.Direction.DESC, "startDate"));
    }

    public Experience getExperienceById(Long id){
        Optional<Experience> experience = experienceRepository.findById(id);
        log.info("experience {}", experience);
        if (experience.isPresent()){
            return experienceRepository.findById(id).orElse(null);
        }
        return null;
    }



    public void addExperience(Experience experience){
        experienceRepository.save(experience);
    }

    public void removeExperience(Long id){
        experienceRepository.deleteById(id);
    }

    public void editExperience(Experience experience){
        experienceRepository.save(experience);
    }
}
