package com.example.resumespring.service;

import com.example.resumespring.model.Interest;

import com.example.resumespring.repository.InterestRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class InterestService {
    private final InterestRepository interestRepository;

    public List<Interest> getInterestsList(){
        return interestRepository.findAll();
    }

    public Interest getInterestById(Long id){
        Optional<Interest> interest = interestRepository.findById(id);
        log.info("interest {}", interest);
        if(interest.isPresent()){
            return interestRepository.findById(id).orElse(null);
        }
        return null;
    }

    public void addInterest(Interest interest){
        interestRepository.save(interest);
    }

    public void removeInterest(Long id){
        interestRepository.deleteById(id);
    }

    public void editInterest(Interest interest){
        interestRepository.save(interest);
    }
}
