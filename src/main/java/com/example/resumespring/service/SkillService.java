package com.example.resumespring.service;

import com.example.resumespring.model.Skill;
import com.example.resumespring.repository.SkillRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class SkillService {

    private final SkillRepository skillRepository;

    public List<Skill> getSkillsList(){
        return skillRepository.findAll();
    }

    public void addSkill(Skill skill){
        skillRepository.save(skill);
    }

    public void removeSkill(Long id){
        skillRepository.deleteById(id);
    }

    public void editSkill(Skill skill){
        skillRepository.save(skill);
    }


}
