package com.example.resumespring.service;

import com.example.resumespring.model.Education;
import com.example.resumespring.repository.EducationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class EducationService {
    private final EducationRepository educationRepository;

    public List<Education> getEducationsList(){
        return educationRepository.findAll(Sort.by(Sort.Direction.DESC, "startDate"));
    }

    public Education getEducationById(Long id){
        Optional<Education> education = educationRepository.findById(id);
        log.info("education {}", education);
        if (education.isPresent()){
            return educationRepository.findById(id).orElse(null);
        }
        return null;
    }

    public void addEducation(Education education){
        educationRepository.save(education);
    }

    public void removeEducation(Long id){
        educationRepository.deleteById(id);
    }

    public void editEducation(Education education){
        educationRepository.save(education);
    }
}
